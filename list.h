/*
 * File:   list.h
 * Author: vladimirskipor
 *
 * Created on 30 Март 2013 г., 19:21
 */

//Двусвязный список. В реализации зациклен через мнимую голову. 

#ifndef list_H
#define    list_H

#include <cstdlib>
#include <algorithm>
#include <exception>

class list {


protected:


    class node {
    public:
        node();

        node(const int, node *, node *);

        node(const node&);

        int value;
        bool is_head;   // warning!! Костыль!!! Как предотвратить блуждание итератора
                        // по кругу и разыменование головы без него?
        node *next;
        node *prev;

        ~node() {
        };
    };


    node *_head;
    size_t _listSize;

public:
    class iterator_out_of_bounds_exception : public std::exception {
        const char *what() const throw();
    };

    class iterator_forbided_dereference_exception : public std::exception {
        const char *what() const throw();
    };

    class erase_from_empty_list_exception : public std::exception {
        const char *what() const throw();
    };

    list();

    list(const list& orig);

    ~list();  ///
    list& operator = (list);


    void swap(list&);

    void clear();

    void push_back(int);

    void pop_back() throw(erase_from_empty_list_exception);

    int back() throw(iterator_forbided_dereference_exception);

    void push_front(int);

    void pop_front() throw(erase_from_empty_list_exception);

    int front() throw(iterator_forbided_dereference_exception);

    size_t size();

    bool empty();


    class iterator : public std::iterator<std::bidirectional_iterator_tag, node> {
    public:
        node *_current;

        iterator();

        iterator(node *);

        iterator(const iterator&);

        iterator& operator = (const iterator&);

        int& operator *() throw(iterator_forbided_dereference_exception);

        iterator& operator ++() throw(iterator_out_of_bounds_exception);

        iterator operator ++(int) throw(iterator_out_of_bounds_exception);

        iterator& operator --() throw(iterator_out_of_bounds_exception);

        iterator operator --(int) throw(iterator_out_of_bounds_exception);

        bool operator ==(const iterator&);

        bool operator !=(const iterator&);

    };


    iterator begin();

    iterator end();

    void erase(const iterator&) throw(erase_from_empty_list_exception);

    iterator insert(const iterator&, int);


};

#endif	/* list_H */

