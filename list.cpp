/*
 * File:   list.cpp
 * Author: vladimirskipor
 *
 * Created on 30 Март 2013 г., 19:21
 */

#include "list.h"



char const *list::erase_from_empty_list_exception::what() const throw() {
    return "Erase from empty list.";
}

char const *list::iterator_forbided_dereference_exception::what() const throw() {
    return "Dereferencing of this iterator is forbided.";
}

char const *list::iterator_out_of_bounds_exception::what() const throw(){
    return "Iterator is out of bounds.";
}

list::list() {
    _listSize = 0;
    _head = new node();
    _head->next = _head->prev = _head;
    _head->is_head = true;

}

list::list(const list& orig) {
    _listSize = 0;
    _head = new node();
    _head->next = _head->prev = _head;
    _head->is_head = true;

    node* pos = orig._head->next;
    while(pos != orig._head){
        push_back(pos->value);
        pos = pos->next;
    }
}

list::~list() {
    while (!empty()) {
        erase(begin());
    }
    delete _head;
}

void list::clear() {
    list trash;
    swap(trash);

}


////////////////////////////////
list::node::node() { //default
    is_head = false;
    value = 0;
    next = 0;
    prev = 0;
};

list::node::node(const int value, node *next, node *prev) {
    this->is_head = false;
    this->value = value;
    this->next = next;
    this->prev = prev;
}

list::node::node(const node &orig) {
    is_head = orig.is_head;
    value = orig.value;
    next = orig.next;
    prev = orig.prev;
}
////////////////////////////

list& list::operator = (list l) { //Внимание, оператору передается правый список по значению, через конструктор копирования,
    //происходит обмен с левым списком, возвращается ссылка на копию правого,
    // у левого списка вызывается деструктор,т.к. кончается область видимости.
    swap(l);
    return *this;
}

void list::swap(list &l) {
    std::swap(_head, l._head);
    std::swap(_listSize, l._listSize);
}

void list::push_back(int value) {
    insert(end(), value);
}

void list::pop_back()  throw(erase_from_empty_list_exception){
    if(empty()) //Warning!! Костыль!! Сделано, для того, чтоб метод бросал только единственный и логичный тип исключения
            //в ином случае, когда метод будет вызван на пустом объекте, сначала итератор выйдет за границу и бросит
            // исключение
        throw erase_from_empty_list_exception();
    erase(--end());
}

int list::back() throw(iterator_forbided_dereference_exception){
    if(empty()) //Warning!! Костыль!! Сделано, для того, чтоб метод бросал только единственный и логичный тип исключения
            //в ином случае, когда метод будет вызван на пустом объекте, сначала итератор выйдет за границу и бросит
            // исключение
        throw iterator_forbided_dereference_exception();
    return *(--end());
}

void list::push_front(int value) {
    insert(begin(), value);
}

void list::pop_front() throw(erase_from_empty_list_exception) {
    erase(begin());
}

int list::front() throw(iterator_forbided_dereference_exception) {
    return *begin();
}

size_t list::size() {
    return _listSize;
}

bool list::empty() {
    return _listSize == 0;
}
////////////////////////////////////

list::iterator::iterator() {
    _current = 0;
}

list::iterator::iterator(node *pointer) {
    _current = pointer;
}

list::iterator::iterator(const iterator& it) {
    _current = it._current;
}

list::iterator& list::iterator::operator = (const list::iterator& rhs) {
    if (this != &rhs) {
        _current = rhs._current;
    }
    return *this;
}

int& list::iterator::operator *() throw(iterator_forbided_dereference_exception) {
    if(_current->is_head){
        throw iterator_forbided_dereference_exception();
    }
    return _current->value;
}

list::iterator& list::iterator::operator ++() throw(iterator_out_of_bounds_exception) {
    if(_current->is_head){
        throw iterator_out_of_bounds_exception();
    }//exception
    _current = _current->next;
    return *this;
}

list::iterator list::iterator::operator ++(int) throw(iterator_out_of_bounds_exception) {
    iterator old = *this;
    ++*this;
    return old;
}

list::iterator& list::iterator::operator --() throw(iterator_out_of_bounds_exception) {      //exception
    if(_current->prev->is_head){
        throw iterator_out_of_bounds_exception();
    }
    _current = _current->prev;
    return *this;
}

list::iterator list::iterator::operator --(int) throw(iterator_out_of_bounds_exception) {
    iterator old = *this;
    --*this;
    return old;
}

bool list::iterator::operator ==(const iterator& it) {
    return _current == it._current;
}

bool list::iterator::operator !=(const iterator& it) {
    return _current != it._current;
}

list::iterator list::begin() {
    return iterator(_head->next);
}

list::iterator list::end() {
    return iterator(_head);
}

list::iterator list::insert(const iterator& it, int value) {
    node *res = it._current->prev = it._current->prev->next = new node(value, it._current, it._current->prev);      //insert before it
    ++_listSize;
    return res;
}

void list::erase(const iterator& it) throw(erase_from_empty_list_exception) {              //erase it    //exception
    if(it._current == _head)
        throw erase_from_empty_list_exception();
    node *old = it._current;
    old->prev->next = old->next;
    old->next->prev = old->prev;
    delete old;
    --_listSize;
}


