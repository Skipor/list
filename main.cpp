
//  main.cpp
//  Circular Doubly Linked List
//
//  Created by Владимир Скипор on 23.03.13.
//  Copyright (c) 2013 Владимир Скипор. All rights reserved.


#define BOOST_TEST_MODULE list_testing
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include "list.h"

struct empty_list {
    empty_list(){
        n = 15;
        check_size = check_sum = 0;
    };
    ~empty_list(){};
    list l;
    int n, check_sum, check_size;
};

struct filled_list {
    list l;
    int check_size, check_sum, n;
    filled_list(){
        check_size = 50;
        check_sum = 0;
        n = 15;
        for(int i = 1; i <= check_size; ++i){
            l.push_back(i);
            check_sum += i;
        }
        
    }
    
    
    
};



BOOST_FIXTURE_TEST_SUITE(empty_list_test, empty_list)

    BOOST_AUTO_TEST_CASE(exceptions)
    {
        BOOST_CHECK_THROW(l.pop_back(), list::erase_from_empty_list_exception);
        BOOST_CHECK_THROW(l.pop_front(), list::erase_from_empty_list_exception);
        BOOST_CHECK_THROW(l.back(), list::iterator_forbided_dereference_exception);
        BOOST_CHECK_THROW(l.front(), list::iterator_forbided_dereference_exception);
        BOOST_CHECK_THROW(*(l.end()), list::iterator_forbided_dereference_exception)
        BOOST_CHECK_THROW(l.end()++, list::iterator_out_of_bounds_exception);
        BOOST_CHECK_THROW(l.begin()--, list::iterator_out_of_bounds_exception);
        BOOST_CHECK_THROW(l.erase(l.end()), list::erase_from_empty_list_exception);
    }

    BOOST_AUTO_TEST_CASE(emptiness_test){
        BOOST_CHECK(l.begin() == l.end());
        BOOST_CHECK(l.empty());
        BOOST_CHECK_EQUAL(l.size(), 0);
        
    }

    BOOST_AUTO_TEST_CASE(push_back_test) {
        l.push_back(n);
        BOOST_CHECK_EQUAL(l.size(), ++check_size);
        BOOST_CHECK_EQUAL(l.back(), n);
        BOOST_CHECK_EQUAL(*(--l.end()), n);
    }
    BOOST_AUTO_TEST_CASE(push_front_test) {
        l.push_front(n);
        BOOST_CHECK_EQUAL(l.size(), ++check_size);
        BOOST_CHECK_EQUAL(l.front(), n);
        BOOST_CHECK_EQUAL(*(l.begin()), n);
    }

    BOOST_AUTO_TEST_CASE(pop_back_test) {
        l.push_back(n);
        l.push_back(n + 1);
        l.pop_back();
        BOOST_CHECK_EQUAL(l.size(), ++check_size);
        BOOST_CHECK_EQUAL(l.back(), n);
        BOOST_CHECK_EQUAL(*(--l.end()), n);
    }

    BOOST_AUTO_TEST_CASE(pop_front_test) {
        l.push_front(n);
        l.push_front(n + 1);
        l.pop_front();
        BOOST_CHECK_EQUAL(l.size(), ++check_size);
        BOOST_CHECK_EQUAL(l.front(), n);
        BOOST_CHECK_EQUAL(*(l.begin()), n);
    }

    BOOST_AUTO_TEST_CASE(insert_test) {
        l.insert(l.end(), n);
        BOOST_CHECK_EQUAL(l.back(), n);
    }

    BOOST_AUTO_TEST_CASE(copy_test){
        list copy(l);
        BOOST_CHECK_EQUAL_COLLECTIONS(l.begin(), l.end(), copy.begin(), copy.end());
    }

    BOOST_AUTO_TEST_CASE(assign_test){
        list copy;;
        copy = l;
        BOOST_CHECK_EQUAL_COLLECTIONS(l.begin(), l.end(), copy.begin(), copy.end());
    }

    BOOST_AUTO_TEST_CASE(swap_test) {
        list copy(l);
        l.push_front(n);
        l.swap(copy);
        l.push_front(n);
        BOOST_CHECK_EQUAL_COLLECTIONS(l.begin(), l.end(), copy.begin(), copy.end());
    }

    BOOST_AUTO_TEST_CASE(clear_test) {
        l.clear();
        BOOST_CHECK(l.empty());
    }



    BOOST_AUTO_TEST_CASE(fill_test) {
        int sum = check_sum;
        int size = check_size;
        for(int i = 1; i <= n; ++i){
            l.push_back(i);
            sum += i;
        }
        
        for(int i = 1; i <= n; ++i){
            sum -= l.back();
            l.pop_back();
        }
        BOOST_CHECK_EQUAL(sum, check_sum);
        BOOST_CHECK_EQUAL(size, check_size);
        
        
    }

BOOST_AUTO_TEST_SUITE_END();



BOOST_FIXTURE_TEST_SUITE(filled_list_test, filled_list)

    BOOST_AUTO_TEST_CASE(check_test) {
        BOOST_CHECK_EQUAL(l.size(), check_size);
        int sum = 0;
        while(!l.empty()) {
            sum += l.front();
            l.pop_front();
        }
        BOOST_CHECK_EQUAL(sum, check_sum);
    }

    BOOST_AUTO_TEST_CASE(erase_test) {
        list::iterator it = l.begin(), left, right;
        for(int i = 1; i <= n; ++i)
            ++it;
        left = it;
        ++it;
        ++it;
        right = it;
        l.erase(--it);
        it = l.begin();
        for(int i = 1; i <= n; ++i)
            ++it;
        BOOST_CHECK(it == left);
        BOOST_CHECK((++it) == right);
        BOOST_CHECK_EQUAL(l.size(), --check_size);
        
    }

    BOOST_AUTO_TEST_CASE(push_back_test) {
        l.push_back(n);
        BOOST_CHECK_EQUAL(l.size(), ++check_size);
        BOOST_CHECK_EQUAL(l.back(), n);
        BOOST_CHECK_EQUAL(*(--l.end()), n);
    }
    BOOST_AUTO_TEST_CASE(push_front_test) {
        l.push_front(n);
        BOOST_CHECK_EQUAL(l.size(), ++check_size);
        BOOST_CHECK_EQUAL(l.front(), n);
        BOOST_CHECK_EQUAL(*(l.begin()), n);
    }

    BOOST_AUTO_TEST_CASE(pop_back_test) {
        l.push_back(n);
        l.push_back(n + 1);
        l.pop_back();
        BOOST_CHECK_EQUAL(l.size(), ++check_size);
        BOOST_CHECK_EQUAL(l.back(), n);
        BOOST_CHECK_EQUAL(*(--l.end()), n);
    }

    BOOST_AUTO_TEST_CASE(pop_front_test) {
        l.push_front(n);
        l.push_front(n + 1);
        l.pop_front();
        BOOST_CHECK_EQUAL(l.size(), ++check_size);
        BOOST_CHECK_EQUAL(l.front(), n);
        BOOST_CHECK_EQUAL(*(l.begin()), n);
    }

    BOOST_AUTO_TEST_CASE(insert_test) {
        l.insert(l.end(), n);
        BOOST_CHECK_EQUAL(l.back(), n);
    }

    BOOST_AUTO_TEST_CASE(copy_test){
        list copy(l);
        BOOST_CHECK_EQUAL_COLLECTIONS(l.begin(), l.end(), copy.begin(), copy.end());
    }

    BOOST_AUTO_TEST_CASE(asign_test){
        list copy;;
        copy = l;
        BOOST_CHECK_EQUAL_COLLECTIONS(l.begin(), l.end(), copy.begin(), copy.end());
    }

    BOOST_AUTO_TEST_CASE(swap_test) {
        list copy(l);
        l.push_front(n);
        l.swap(copy);
        l.push_front(n);
        BOOST_CHECK_EQUAL_COLLECTIONS(l.begin(), l.end(), copy.begin(), copy.end());
    }

    BOOST_AUTO_TEST_CASE(clear_test) {
        l.clear();
        BOOST_CHECK(l.empty());
    }



    BOOST_AUTO_TEST_CASE(fill_test) {
        int sum = check_sum;
        int size = check_size;
        for(int i = 1; i <= n; ++i){
            l.push_back(i);
            sum += i;
        }
        
        for(int i = 1; i <= n; ++i){
            sum -= l.back();
            l.pop_back();
        }
        BOOST_CHECK_EQUAL(sum, check_sum);
        BOOST_CHECK_EQUAL(size, check_size);
        
        
    }



BOOST_AUTO_TEST_SUITE_END();









